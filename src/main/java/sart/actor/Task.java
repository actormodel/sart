package sart.actor;

import java.util.concurrent.ForkJoinTask;

import org.hewitt.actor.Messenger;

public abstract class Task extends ForkJoinTask<Void>{
	
	Messenger m() { return null; }
	
	public abstract void tick();
	
	@Override
	protected boolean exec() {
		try {
			tick();	
		} catch(Exception e) { e.printStackTrace(); }
		return true;
	}
	
	protected Worker worker() {
		return Worker.current();
	}
	
	@Override
	public Void getRawResult() { return null; }

	@Override
	protected void setRawResult(Void value) { }

	static class Message extends Task {
		final Envelope envelope;
		
		Message(Envelope envelope) {
			this.envelope = envelope;
		}
		
		@Override
		public void tick() {
			worker().execute(this);
			worker().push(envelope);
			envelope.m.process();
			worker().pop().flush();
			envelope.target.free();
		}
		
		@Override
		Messenger m() {
			return envelope.m;
		}
		
		private static final long serialVersionUID = 1084513300052315685L;
	}
	
	private static final long serialVersionUID = -4899097355891852595L;
}
