package sart.actor;

import org.hewitt.actor.ActorSystem;
import org.hewitt.actor.Messenger;

/*
 * Данный класс являет родительским классом для всех определяемых акторов в системе.
 * При созданни нового актора-класса, должен расширятся данным.
 * 
 */
public abstract class BasicActor implements ActorSystem<SAddress, BasicActor> {

	/*
	 * Возвращает конструкт-поведения по ключу. Если с данным ключом не существует поведения, тогда создает его.
	 * Главное понимать, что определять поведения, для актора, можно только вовремя его установки, при вызове метода define.
	 * @return конструктор поведения
	 */
	/*
	 * DEVNOTE 
	 * В консруктор BehaviorBuild параметр Object receiver передаем this.
	 * Программирование поведения происходит исходя исполняемого контекста
	 * 
	 * TODO Ограничить порождения поведения на этапе define, при попытке вызова конструктора из другого состояние генерировать исключение.
	 */
	public BehaviorBuilder behavior(Object key) {
		return new BehaviorBuilder(this, key);
	}
	
	public void send(Object...m) {
		send(M(m), 0);
	}
	/*
	 * Recursion message
	 * Actor send message to it self
	 * @m messenger for sending
	 */
	public void send(Messenger m) {
		send(m, 0);
	}
	
	public void send(Messenger m, long delay) {
		Worker.current().out(Worker.current().target(), m, delay);
	}
	
	public void define() { }
		
	public SAddress sender() {
		return Worker.current().sourceref();
	}
	
	@Override
	public void send(SAddress target, Messenger m) { 
		Worker.current().out(target.record, m, 0);
	}

	@Override
	public void become(Object... behaviors) { 
		Worker.current().target().become(behaviors);
	}

	@Override
	public SAddress create(Object address, BasicActor instance) { 
		return Worker.current().create(address, instance);
	}
	
	public static Messenger M(Object...m) {
		return new Message(m);
	}
	
	//CR, M, RQ, RP, CMP
	
}
