package sart.actor;

import org.hewitt.actor.Messenger;
import org.hewitt.actor.Reference;

public class SAddress implements Reference {
	//package
	Record record;
	
	SAddress(Record record) {
		this.record = record;
	}
	
	public void send(Object...m) {
		send(new Message(m), 0);
	}
	
	@Override
	public void send(Messenger m) {
		send(m, 0);
	}

	public void send(Messenger m, long delay) {
		Worker.current().out(record, m, delay);
	}
}
