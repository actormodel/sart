package sart.actor;

import java.util.concurrent.atomic.AtomicLong;

import org.hewitt.actor.Messenger;

public class Envelope {
	public final Record source;
	
	public final Record target;
	
	public final Messenger m;
	
	public final long time = System.currentTimeMillis();
	
	public final long tag = next();
	
	public final long delay;
	
	Envelope(Record source, Record target, Messenger m) {
		this(source, target, m, 0);
	}
	
	Envelope(Record source, Record target, Messenger m, long delay) {
		this.source = source;
		this.target = target;
		this.m = m;
		this.delay = delay;
	}
	
	long rtime() {
		return time + delay;
	}
	
	private final static AtomicLong generator = new AtomicLong();
	
	private static long next() {
		return generator.getAndIncrement() & Long.MAX_VALUE;
	}
}
