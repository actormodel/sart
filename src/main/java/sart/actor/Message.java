package sart.actor;

import java.util.Arrays;
import java.util.Optional;

import org.hewitt.actor.Messenger;
import org.pmw.tinylog.Logger;

public class Message implements Messenger, WorkerContext {
	private Object[] content;

	public Message(Object[] content) {
		this.content = content;
	}
	
	@Override
	public void activation(Messenger m) {
	}

	@Override
	public void arrival(Messenger m) {
		target().
			process(record -> {
				//System.out.println("arrival");
			});
	}

	@Override
	public void process() {
		target().
			process(record -> {
				final Optional<Record.Handler> handler = record.find(content);
				if(handler.isPresent()) {
					handler.get().act(content);
				} else
					Logger.warn("unhandled messages actor = `{}` message = {}", record, Arrays.toString(content));
			});
	}

	@Override
	public Messenger and(Messenger m) {
		return new Messages(this, m);
	}

	@Override
	public void activate(Object m) {
		if(m instanceof Envelope) {
			final Envelope e = (Envelope) m;
		} else
			System.out.println("wrong activator");
	}

}
