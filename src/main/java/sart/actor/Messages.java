package sart.actor;

import java.util.ArrayList;

import org.hewitt.actor.MessageSequence;
import org.hewitt.actor.Messenger;

public class Messages implements MessageSequence {
	private final ArrayList<Messenger> m = new ArrayList<>();
	
	Messages(Messenger m0, Messenger m1) {
		m.add(m0);
		m.add(m1);
	}
	
	Messages() {
		
	}
	
	@Override
	public void activation(Messenger m) {
		
	}

	@Override
	public void arrival(Messenger m) {
		
	}

	@Override
	public void process() {
		/*
		 * record.lock(
		 * 	r -> {
		 * 		r.target
		 * 		execute(new Messages(task);
		 *  }
		 * );
		 * 
		 */
	}

	@Override
	public Messenger and(Messenger m) {
		return m.and(m);
	}

	@Override
	public void activate(Object m) {	
		
	}

}
