package sart.actor;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.ForkJoinPool.ForkJoinWorkerThreadFactory;

import java.util.concurrent.ForkJoinWorkerThread;

public class Model extends BasicActor implements ForkJoinWorkerThreadFactory {
	private final Record record = new Record(this, null, "actor-system");
		
	final Scheduler delayService = new Scheduler();
	
	private BasicActor root;
	
	public static boolean debug = true;
	
	@Override
	public void define() {
		if(root != null) {
			create("root", root);
		} else
			System.out.println("TODO not define root actor");//TODO handle
	}
	
	public static void launch(BasicActor root) {
		System.setProperty("java.util.concurrent.ForkJoinPool.common.threadFactory", Model.class.getName());
		System.setProperty("tinylog.level", "info");
		final Model m = (Model) ForkJoinPool.commonPool().getFactory();
		m.root = root;
		ForkJoinPool.commonPool().execute(m.delayService);
		ForkJoinPool.commonPool().execute(new Task.Message(new Envelope(m.record, m.record, new M.Create())));
	}

	@Override
	public ForkJoinWorkerThread newThread(ForkJoinPool pool) {
		return new Worker(pool);
	}
	public static class Scheduler extends ForkJoinTask<Void>{
		private LinkedList<Envelope> schedule = new LinkedList<>();
		
		public void add(Envelope envelope) {
			synchronized (this) {
				schedule.add(envelope);
			}
		}
		
		@Override
		protected boolean exec() {
			synchronized (this) {
				final Iterator<Envelope> it = schedule.iterator();
				while(it.hasNext()) {
					Envelope env = it.next();
					if(System.currentTimeMillis() >= env.rtime()) {
						it.remove();
						env.target.in(env);
					}
				}
			}
			getPool().execute(this);
			return false;
		}

		@Override
		public Void getRawResult() { return null; }
		@Override
		protected void setRawResult(Void value) { }

		private static final long serialVersionUID = -6743420317471674704L;
	}
}
