package sart.actor;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.pmw.tinylog.Logger;

public class BehaviorBuilder {
	private static final Object[] EMPTY = new Object[]{};
	
	private final Object receiver;//object method handler invoke
	
	private final Object key;//behavior key
	
	private final MethodHandles.Lookup lookup = MethodHandles.lookup();
	
	private Record.Behavior behavior;
	
	BehaviorBuilder(Object receiver, Object key) {
		this.receiver = receiver;
		this.key = key;
	}

	private BehaviorBuilder register(String pattern, String predicate, Class<?>[] signature) {
		if(behavior == null)
			behavior = Worker.current().target().behavior(key);
		final List<Token> list = parse(pattern);
		try {	
			switch((int)list.stream().filter(t -> t.type == Token.Type.OR).count()) {
			case 0:
				behavior.register(
					new Record.PredicateHandler(
							lookup.bind(receiver, predicate, MethodType.methodType(boolean.class, signature)), 
							lookup.bind(receiver, list.get(0).value, MethodType.methodType(Void.TYPE, signature)), 
							null, 
							signature
						)
					);
				break;
			case 1: 
				behavior.register(
					new Record.PredicateHandler(
							lookup.bind(receiver, predicate, MethodType.methodType(boolean.class, signature)), 
							lookup.bind(receiver, list.get(0).value, MethodType.methodType(Void.TYPE, signature)), 
							lookup.bind(receiver, list.get(2).value, MethodType.methodType(Void.TYPE, signature)), 
							signature
						)
					);
				break;
			default: throw new RuntimeException("must contain only one | : " + pattern);
			}
		} catch(Exception e) {
			Logger.error(e);
		} 
		return this;
	}
	
	//TODO check matches range
	private BehaviorBuilder register(String name, Class<?>[] signature, Object[] matches) {
		try {	
			if(behavior == null)
				behavior = Worker.current().target().behavior(key);
			MethodHandle m = 
					matches.length > 0 
					? lookup.bind(receiver, name, MethodType.methodType(Void.TYPE))
					: lookup.bind(receiver, name, MethodType.methodType(Void.TYPE, signature));
			behavior.register(new Record.MessageHandler(m, signature, matches));
		} catch(Exception e) {
			Logger.error(e);
		} 
		return this;
	}
	
	/*
	 * Create message case for passing message
	 * @name - method name in object receiver
	 * @return this
	 */
	public BehaviorBuilder Case(String name, Class<?>...signature) {
		return register(name, signature, EMPTY);
	}
	
	/*
	 * 
	 * matches.length >= signature.length
	 * && matches.length % signature.length == 0
	 * 
	 */
	public BehaviorBuilder Case(String name, Class<?>[] signature, Object[] matches) {
		return register(name, signature, matches);
	}
	
	public static Class<?>[] tsig(Class<?>...classes) {
		return classes;
	}
	
	public static Object[] vsig(Object...values) {
		return values;
	}

	public BehaviorBuilder Case(String pattern, String predicate, Class<?>...signature) {
		return register(pattern, predicate, signature);
	}
	
	private static List<Token> parse(String name) {
		final ArrayList<Token> list = new ArrayList<>();
		int state = 0;
		final StringBuffer buffer = new StringBuffer();
		for(int pos = 0 ; pos < name.length() ; pos++) {
			final char peek = name.charAt(pos);
			switch(state) {
			case 0: { //NULL STATE 
				if(Character.isLetterOrDigit(peek)) {
					state = 1; //set word state
					buffer.append(peek);
				} else if(peek == '&' || peek == '|') {
					throw new RuntimeException("cant start with & or | " + name);
				} else
					throw new RuntimeException("parser error wrong char: " + peek + " pos: " + pos + " string: " + name);
				break;
			}
			case 1: {//WORD state
				if(Character.isLetterOrDigit(peek)) {
					buffer.append(peek);
				} else if(peek == '&') {
					state = 0;
					list.add(new Token(Token.Type.ID, buffer.toString()));
					buffer.setLength(0);
					list.add(new Token(Token.Type.AND, "&"));
				} else if(peek == '|') {
					state = 0;
					list.add(new Token(Token.Type.ID, buffer.toString()));
					buffer.setLength(0);
					list.add(new Token(Token.Type.OR, "|"));
				}
				break;
			}
			}
		}
		if(state == 0)
			throw new RuntimeException("parse error: must end with Token.Type.ID : " + name);
		list.add(new Token(Token.Type.ID, buffer.toString()));
		return list;
	}
	
	
	private static class Token {
		private enum Type { ID, OR, AND }
		final Type type;
		final String value;
		Token(Type type, String value) {
			this.type = type;
			this.value = value;
		}
		@Override
		public String toString() { return value; }
	}
}
