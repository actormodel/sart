package sart.actor;

interface WorkerContext {
	
	default Record target() {
		return Worker.current().target();
	}
	
	default Record source() {
		return Worker.current().source();
	}
	
}
