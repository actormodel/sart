package sart.actor;

import java.lang.invoke.MethodHandle;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Optional;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

import org.hewitt.actor.Messenger;
import org.pmw.tinylog.Logger;

public class Record {
	private final BasicActor instance;
	
	private volatile int state = ST_DEFINE;
	
	private Record system;//parent system, in wich contains this record
	
	private String address;//addres of current actor 
	
	private Messenger M;//current messenger
	
	private Envelope target;
	
	private final HashMap<String, Record> childs = new HashMap<>();
	
	private final LinkedList<Envelope> mailbox = new LinkedList<>();

	private final HashMap<Object, Behavior> btable = new HashMap<>();
	
	private final LinkedList<Behavior> behavior = new LinkedList<>();

	final SAddress ref = new SAddress(this);
	
	Record(BasicActor instance, Record system, String address) {
		this.instance = instance;
		this.system = system;
		this.address = address;
	}
	
	
	private final AtomicInteger lock = new AtomicInteger(0);
	
	
	/*
	 * Note: RU Данный метод вызывается при обработке DefineTask
	 * 
	 */
	public synchronized void define() {
		Worker.current().guard(this);
		Logger.debug("defining actor {}", this);
		if(state == ST_DEFINE) {
			
			instance.define();
		} else
			throw new RuntimeException("TODO Record.define()");
	} 
	
	/*
	 * 
	 * NOTE: RU Порождения акторов в системе возможно, только во время обработки сообщения, данной системы.
	 */
	SAddress create(String address, BasicActor instance) {
		Worker.current().guard(this);
		synchronized(childs) {
			final Record child = new Record(instance, this, address);
			if(childs.putIfAbsent(address, child) == null) {
				Logger.debug("Creating actor {} with adresss {}", instance.getClass().getName(), address);
				ForkJoinPool.commonPool().execute(new Task.Message(new Envelope(this, child, new M.Create())));
			} else {
				Logger.error("Actor with current address {}/{} already exists", this, address);
				throw new RuntimeException("TODO Record.create()");//TODO
			}
			return child.ref;
		}
	}
	
	void process(Consumer<Context> f) { 
		f.accept(new Context());
	}
	
	synchronized void in(Envelope e) {
		Logger.debug("actor {} incoming message {} with state {}", this, e, state);
		//TODO handle from messenger
		
		if(state == ST_FREE) {
			target = e;
			ForkJoinPool.commonPool().execute(new Task.Message(e));
			state = ST_PROCESS;
		} else
			mailbox.add(e);
	} 
	
	void out(Record target, Messenger m, long delay) { 
		Worker.current().guard(this);
	}
	
	synchronized void free() { 
		Worker.current().guard(this);
		Logger.debug("Actor free {} ", this);
		if(!mailbox.isEmpty()) {
			target = mailbox.removeFirst();
			ForkJoinPool.commonPool().execute(new Task.Message(target));
			state = ST_PROCESS;
		} else
			state = ST_FREE;
	}

	private static final int ST_DEFINE = 0;
	private static final int ST_FREE = 1;
	private static final int ST_LOCK = 2;
	private static final int ST_PROCESS = 3;

	@Override
	public String toString() {
		return (system != null ? system + "/" : "")  + address;
	}
	
	//----------------behavior control
	void become(Object...keys) {
		Worker.current().guard(this);
		behavior.clear();
		for(Object key : keys)
			if(btable.containsKey(key))
				behavior.add(btable.get(key));
	}
	
	Behavior behavior(Object key) {
		Worker.current().guard(this);
		if(!btable.containsKey(key))
			btable.put(key, new Behavior(key));
		return btable.get(key);
	}

	
	/*
	 * Find first match for handling behavior
	 */
	private Optional<Handler> find(Object[] content) {
		Handler ret = null;
		for(Behavior b : behavior) {
			ret = b.find(content);
			if(ret != null)
				break;
		}
		return Optional.ofNullable(ret);
	}

	static class Behavior {
		final Object key;
		
		final LinkedList<Handler> handlers = new LinkedList<>();
		
		Behavior(Object key) {
			this.key = key;
		}
		
		void register(Handler handler) {
			handlers.add(handler);
		}
		
		Handler find(Object[] values) {
			for(Handler handler : handlers)
				if(handler.match(values))
					return handler;
			return null;
		}
	}

	static interface Handler {
		boolean match(Object[] values);
		
		void act(Object[] values);
		
		default boolean check(Class<?> c0, Class<?> c1) {
			if(c0.isPrimitive()) {
				if(c1 == Character.class) {
					c1 = char.class;
				} else if(c1 == Boolean.class) {
					c1 = boolean.class;
				} else if(c1 == Byte.class) {
					c1 = byte.class;
				} else if(c1 == Short.class) {
					c1 = short.class;
				} else if(c1 == Integer.class) {
					c1 = int.class;
				} else if(c1 == Long.class) {
					c1 = long.class;
				} else if(c1 == Float.class) {
					c1 = float.class;
				} else if(c1 == Double.class) {
					c1 = double.class;
				}	
				return c0 == c1;
			} else {
				return c0.isAssignableFrom(c1);
			}
		}
		
		default boolean check(Object[] content, Class<?>[] signature) {
			if(signature.length != content.length)
				return false;
			for(int i = 0 ; i < content.length ; i++)
				if(!check(signature[i], content[i].getClass()))
					return false;
			return true;
		}
	}
	
	static class PredicateHandler implements Handler {
		private final MethodHandle predicate;
		private final MethodHandle left;
		private final MethodHandle right;
		private final Class<?>[] signature;
		public PredicateHandler(MethodHandle predicate, MethodHandle left, MethodHandle right, Class<?>[] signature) {
			this.predicate = predicate;
			this.left = left;
			this.right = right;
			this.signature = signature;
		}
	
		@Override
		public boolean match(Object[] content) {
			try {
				if(check(content, signature)) {
					if(right == null) {
						return (Boolean) predicate.invokeWithArguments(content);
					} else
						return true;
				} else
					return false;
			} catch (Throwable e) {
				Logger.error(e);
				return false;
			}

		}

		@Override
		public void act(Object[] content) {
			try {
				if(right != null) {
					if((Boolean) predicate.invokeWithArguments(content)) 
						left.invokeWithArguments(content);
					else
						right.invokeWithArguments(content);
				} else
					left.invokeWithArguments(content);
			} catch(Throwable e) { Logger.error(e); }
		}
	}
	
	static class MessageHandler implements Handler{
		final MethodHandle mh;
		final Class<?>[] signature;
		final Object[] matches;
		
		MessageHandler(MethodHandle mh, Class<?>[] signature, Object[] matches) {
			if(signature == null || matches == null || mh == null)
				throw new NullPointerException();
			this.mh = mh;
			this.signature = signature;
			this.matches = matches;
		}
		
		@Override
		public boolean match(Object[] values) {
			if(check(values, signature)) {
				if(matches.length > 0) {
					for(int i = 0 ; i < matches.length / values.length ; i++) {
						int count = 0;
						for(int n = 0 ; n < values.length ; n++)
							if(values[n].equals(matches[i*values.length + n]))
								count++;
							else
								break;
						if(count == values.length)
							return true;
					}
					return false;
				} else 
					return true;
			} else
				return false;
		}
		
		@Override
		public void act(Object[] values) {
			try {
				if(matches.length == 0)
					mh.invokeWithArguments(values);
				else
					mh.invoke();
			} catch (Throwable e) {
				Logger.error(e);
			}
		}
	}
	
	class Context {
		
		void define() {
			if(state == ST_DEFINE) {
				instance.define();
			} else
				throw new RuntimeException("define problem");//TODO
		}
		
		Optional<Handler> find(Object[] content) {
			return Record.this.find(content);
		}
	}
	//-----------------behavior control
}