package sart.actor;

import org.hewitt.actor.Messenger;

class M {

	static class Message implements Messenger, WorkerContext {

		@Override
		public void activation(Messenger m) { }

		@Override
		public void arrival(Messenger m) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void process() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public Messenger and(Messenger m) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void activate(Object m) {
			if(m instanceof Envelope) {
				final Envelope e = (Envelope) m;
			} else
				System.out.println("wrong activator " + m);
		} 
		
	}
	
	static class Create extends Message {

		@Override
		public void process() {
			target().process(record -> {
				record.define();
			});
		}
	}
}
