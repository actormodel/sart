package sart.actor;

import java.util.LinkedList;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinWorkerThread;

import org.hewitt.actor.Messenger;

public class Worker extends ForkJoinWorkerThread {
	private Context context;
	
	private Task task;
	
	Worker(ForkJoinPool pool) {
		super(pool);
		setDaemon(false);
	}

	void guard(Record record) {
		if(record != context.target)
			throw new RuntimeException("TODO Worker.guard(record)");
	}
	
	public void execute(Task task) {
		this.task = task;
	}
	
	public Context push(Envelope env) {
		return (context = new Context(env));
	}
	
	public Context push(Record source, Record target) {
		return (context = new Context(source, target));
	}
	
	public Context pop() {
		return context;
	}
	
	public static class Context {
		final Record source;
		
		final Record target;
		
		final LinkedList<Envelope> buf = new LinkedList<>();
		
		int flags = 0;
		
		Context(Envelope env) {
			this(env.source, env.target);
		}
		
		Context(Record source, Record target) {
			this.source = source;
			this.target = target;
		}
		
		public void flush() {
			buf.forEach(e -> {
				if(e.rtime() > System.currentTimeMillis())
					((Model) ForkJoinPool.commonPool().getFactory()).delayService.add(e);
				else
					e.target.in(e);
			});
		}
	}
	
	public static Worker current() {
		return (Worker) currentThread();
	}
	
	SAddress create(Object address, BasicActor instance) {
		return context.target.create(address.toString(), instance);
	}

	void out(Record target, Messenger m, long delay) {
		final Envelope envelope = new Envelope(context.target, target, m, delay);
		context.buf.add(envelope);
		if(task.m() != null) {
			task.m().activate(envelope);
		} else
			System.out.println("out activate problem");//TODO out activate problem
		
	}
	
	Record target() {
		return context.target;
	}
	
	Record source() {
		return context.source;
	}

	public SAddress sourceref() {
		return context.source.ref;
	}

	
	
}
