package org.hewitt.actor;

public interface Acquaintances<R> {

	public R sender();
	
	public R self();
	
	public Object content();
	
}
