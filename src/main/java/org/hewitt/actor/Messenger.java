package org.hewitt.actor;

public interface Messenger {
	
	@Deprecated 
	public void activation(Messenger m);
	
	public void activate(Object m);
	
	public void arrival(Messenger m);
	
	public void process();
	
	default boolean isSeq() { return false; }
	
	public Messenger and(Messenger m);
	
}
