package org.hewitt.actor;

public interface Actor<R extends Reference> {

	public void send(R target, Messenger m);
	
	public void become(Object...behaviors);
	
}
