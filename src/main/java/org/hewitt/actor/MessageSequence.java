package org.hewitt.actor;

public interface MessageSequence extends Messenger {

	default boolean isSeq() { return true; }
	
	default boolean hasNext() { return false; }
	
	default boolean next() { return false; }
}
