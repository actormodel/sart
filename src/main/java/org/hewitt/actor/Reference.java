package org.hewitt.actor;

public interface Reference {

	public void send(Messenger m);
	
}
