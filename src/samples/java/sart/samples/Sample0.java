package sart.samples;

import sart.actor.BasicActor;
import sart.actor.BehaviorBuilder;
import sart.actor.Model;
import sart.actor.SAddress;

/*
 * В данном примере демонстрируются принцып работы message sequence
 * Message Sequence гарантирует последовательность обработки сообщений.
 * Актор обробатывает всю последовательность, и затем освобождается.
 * 
 * |[variant 0]
 * | A0 : send(A1, m0);
 * | A0 : send(A1, m1);
 * 
 * | A1.in(m0)
 * | ... 
 * | A1.in(m1)
 * | ...
 * | A1.process(m0)
 * | A1.free
 * | ...
 * | A1.process(m1)
 * | A1.free
 * | ...
 * 
 * 
 * |[variant 1]
 * | A0 : send(A1, m0, m0)
 * 
 * | A1.in(seq(m0, m1))
 * | ...
 * | A1.process(m0)
 * | ...
 * | A1.process(m1)
 * | A1.free
 * 
 * [...] - any other action in system
 * 
 * NOTEDOC Обработка цепочки сообщения происходит в прямом порядке.
 * 
 * E0 --> E1
 * 
 * где последовательный вызов send не гарантирует последовательность обработки, так как вызов двух методов send не может происходить в один момент времени.
 * Для двух вызовов создается временное окно, вовремя этого окна актор может получить сообщение, которое может нарушить порядок обработки, который виден на
 * определенном участке кода, алгоритм не делает, то что от него ожидают.
 * 
 * 
 * send(M(1, 2)) != send(M(1) ; send(M(2))
 * 
 * 
 * Возможны два варианта 
 * My name is Bob. I have one and two
 * My name is Sam. I have two and one
 * 
 * My name is Sam. I have two and one
 * My name is Bob. I have one and two
 * 
 * Система не гарантирует порядок обработки сообщений в порядки их порождения во времени.
 * NOTE Что-то может оказаться быстрее скорости света...
 */
public class Sample0 extends BasicActor {
	
	@Override
	public void define() {
		SAddress actor = create("bob", new A("Bob"));
		actor.send(1);
		actor.send(2);
		actor.send("who");
		actor = create("sam", new A("Sam"));
		//между call0 и call1 может быть доставленно другое сообщение.
		actor.send(2);//call 0
		actor.send(1);//call 1
		actor.send("who");
		
		//TODO сделать пример с искусственой ситуацией, где происходит обработка "вклинившегося" сообщения.
		//NOTE "Вклинившееся" сообщенние - сообщение которое нарушает порядок алгоритма, с точки зрения программиста.
		//Если существует последовательная отправка сообщений, с точки зрения программиста, эти сообщения обработаются 
		//последовательно, но система не гарантирует этого, так как может быть другой актор отправляет сообщение, и 
		//система может доставить это сообщения между вызовами, наблюдаемого участка кода, и сообщение "вклинивается" в "естественный" порядок.
	}
	
	public static class A extends BasicActor {
		private String name;
		
		private String first;
		
		private String second;
		
		public A(String name) {
			this.name = name;
		}
		
		@Override
		public void define() {
			behavior("IDLE0").
				Case("wait0", int.class);
			behavior("IDLE1").
				Case("wait1", int.class);
			behavior("WHO").
				Case("who", BehaviorBuilder.tsig(String.class), BehaviorBuilder.vsig("who"));
			become("IDLE0");
		}
		
		public void wait0(int n) {
			switch(n) {
			case 1: first = "one"; become("IDLE1"); break;
			case 2: first = "two"; become("IDLE1"); break;
			default:System.out.println("wait 1 or 2 receive : " + n);
			}
		}
		
		public void wait1(int n) {
			switch(n) {
			case 1: second = "one"; become("WHO"); break;
			case 2: second = "two"; become("WHO"); break;
			default: System.out.println("wait 1 or 2 receive : " + n);
			}
		}
		
		public void who() {
			System.out.println("My name is " + name + ". I have " + first + " and " + second);
		}
	}
	public static void main(String...args) {
		Model.launch(new Sample0());
	}
}