package sart.samples;

import sart.actor.BasicActor;
import sart.actor.Model;

/*
 * В примере показываются способы задания поведения для актора.
 * 
 * При задании поведения главное помнить, что обрабатывается первое соответсвие по шаблону. 
 *
 * Тоесть если задать такое поведение
 * behavior("").
 * 	Case("test0", Object.class).
 * 	Case("test1", String.class);
 * 
 * для кортежа с размером один, будет всегда срабатывать test0, так как Object является родительским классом, для всех объектов.
 * void test0(Object o)
 * void test1(String s)
 */
public class BehaviorSamples extends BasicActor {
	/*
	 * 
	 */
	public static class A extends BasicActor {
		@Override
		public void define() {
			behavior("IDLE").
				Case("hello_string", String.class).
				Case("hello_int", int.class);
			become("IDLE");
		}
		
		public void hello_string(String value) {
			System.out.println("A: hello string " + value);
		}
		
		public void hello_int(int value) {
			System.out.println("A: hello int " + value);
		}
	}
	
	public static class B extends BasicActor {
		@Override
		public void define() {
			behavior("IDLE").
				Case("hello");
		}
	}
	
	@Override
	public void define() {
		
	}
	
	public static void main(String...args) {
		Model.launch(new A());
	}
}
