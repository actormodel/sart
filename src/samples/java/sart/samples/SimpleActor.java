package sart.samples;

import sart.actor.BasicActor;
import sart.actor.Model;

public class SimpleActor extends BasicActor {

	@Override
	public void define() {
		behavior("IDLE").
			Case("print", String.class);
		become("IDLE");
		send("hello world");
	}
	
	public void print(String value) {
		System.out.println(value);
	}
	
	public static void main(String...args) {
		Model.launch(new SimpleActor());
	}
}
