package sart.samples;

import sart.actor.BasicActor;
import sart.actor.Model;

/*
 * Unhandled message
 */
public class Sample1 extends BasicActor {

	@Override
	public void define() {
		behavior("IDLE").
			Case("test", String.class);
		become("IDLE");
		send("hello world");
		send(1);
	}
	
	public void test(String test) {
		System.out.println(test);
	}
	
	public static void main(String...args) {
		Model.launch(new Sample1());
	}
}
