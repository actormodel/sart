package sart.samples;

import sart.actor.BasicActor;
import sart.actor.Model;
import sart.actor.SAddress;

public class PingPong extends BasicActor {
	
	@Override
	public void define() {
		create("pong", new Pong(create("ping", new Ping())));
	}
	
	public static class Pong extends BasicActor {
		private SAddress target;
		
		public Pong(SAddress target) {
			this.target = target;
		}
		
		@Override
		public void define() {
			behavior("IDLE").
				Case("pong", Object.class);
			become("IDLE");
			target.send(M("pong"));
		}
		
		public void pong(Object m) {
			System.out.println("pong receive " + m);
			sender().send(M("pong"));
		}
	}
	
	public static class Ping extends BasicActor {
		@Override
		public void define() {
			behavior("IDLE").
				Case("ping", Object.class);
			become("IDLE");
		}
		
		public void ping(Object m) {
			System.out.println("ping receive " + m);
			sender().send(M("ping"));
		}
	}
	
	public static void main(String...args) {
		Model.launch(new PingPong());
	}
	
}
