package sart.samples;

import sart.actor.BasicActor;
import sart.actor.Model;

public class Hello extends BasicActor {

	@Override
	public void define() {
		System.out.println("test");
	}
	
	public static void main(String...args) {
		Model.launch(new Hello());
	}
}
