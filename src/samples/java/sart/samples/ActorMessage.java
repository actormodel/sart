package sart.samples;

import sart.actor.BasicActor;
import sart.actor.Model;

public class ActorMessage extends BasicActor {

	@Override
	public void define() {
		behavior("IDLE").
			Case("f", char.class).
			Case("f", byte.class).
			Case("f", short.class).
			Case("f", int.class).
			Case("f", long.class).
			Case("f", boolean.class).
			Case("f", float.class).
			Case("f", double.class).
			Case("f", String.class).
			Case("f", Object.class);
		become("IDLE");
		
		for(char c : "hello world".toCharArray())
			send(c);
		send(true);
		send(false);
		send(0.0f);
		send(0.121324f);
		send("hello world");
		send(3.14d);
		send((byte)128);
		send((short)128);
		send(128);
		send((long)128);
		send(new RuntimeException("object handle"));
	}

	public void f(boolean x) { System.out.println("hello boolean " + x); }
	public void f(char c) { System.out.println("hello char " + c); }
	public void f(byte x) { System.out.println("hello byte " + x); }
	public void f(short x) { System.out.println("hello short " + x); }
	public void f(int x) { System.out.println("hello int " + x); }
	public void f(long x) { System.out.println("hello long " + x); }
	public void f(float x) { System.out.println("hello float " + x); } 
	public void f(double x) { System.out.println("hello double " + x); } 
	public void f(String x) { System.out.println("hello string " + x); }
	public void f(Object x) { System.out.println("hello object " + x); }
	
	public static void main(String...args) {
		Model.launch(new ActorMessage());
	}
}
