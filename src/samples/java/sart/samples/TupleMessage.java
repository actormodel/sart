package sart.samples;

import sart.actor.BasicActor;
import sart.actor.Model;

public class TupleMessage extends BasicActor {

	@Override
	public void define() {
		behavior("IDLE").
			Case("x", int.class).
			Case("xy", int.class, int.class).
			Case("xyz", int.class, int.class, int.class);
		become("IDLE");
		send(1);
		send(1, 2);
		send(1, 2, 3);
	}
	
	public void x(int x) {
		System.out.println("(x) : " + x); 
	}
	
	public void xy(int x, int y) {
		System.out.println("(x, y) : " + x + ", " + y); 
	}
	
	public void xyz(int x, int y, int z) {
		System.out.println("(x, y, z) : " + x + ", " + y + ", " + z); 
	}
	
	public static void main(String...args) {
		Model.launch(new TupleMessage());
	}
}
