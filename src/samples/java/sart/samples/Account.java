package sart.samples;

import sart.actor.BasicActor;
import sart.actor.Model;

public class Account extends BasicActor {
	private final String login;
	private final String password;
	public Account(String login, String password) {
		this.login = login;
		this.password = password;
	}
	
	@Override
	public void define() {
		behavior(State.Free).
			Case("login", String.class, String.class);
		behavior(State.Login).
			Case("login0", String.class, String.class).
			Case("play");
		
		become(State.Free);
		send(M("root", "root1"));
		send(M("root", "root"));
		send(M("root", "root"));
	}
	
	public void login0(String l, String p) {
		System.out.println("account in use");
	}
	
	public void login(String l, String p) {
		if(login.equals(l) && password.equals(p)) {
			become(State.Login);
			System.out.println("login success");
		} else
			System.out.println("login failed");
	}
	
	enum State { Free, Login, Play }
	
	public static void main(String...args) {
		Model.launch(new Account("root", "root"));
	}
	
}
