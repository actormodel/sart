package sart.samples;

import sart.actor.BasicActor;
import sart.actor.Model;

import static sart.actor.BehaviorBuilder.*;

public class MultiargsMessage extends BasicActor {
	
	@Override
	public void define() {
		behavior("IDLE").
			Case("test", int.class, String.class).
			Case("test", int.class).
			/*
			 * При задании шаблона соответсвия, алгоритм поиска метода игнорирует сигнатуру вызова.
			 * MethodHandler method = lookup.bind(receiver, void.class, void.class)
			 * vsig(v0, v1,.... vN)
			 * tsig(t0, t1,.... tM)
			 * M <= N
			 * N % M == 0
			 */
			Case("test", tsig(int.class, int.class), vsig(1, 1, 2, 2, 1, 2)).
			Case("test", int.class, int.class);
		become("IDLE");
		send(1);
		send(1, "hello");
		send(1, 1);
		send(2, 2);
		send(1, 2);
		send(2, 1);
	}
	
	public void test(int i) { System.out.println("test("+i+")"); } 
	public void test(int i, String s) { System.out.println("test("+i+","+s+")"); }
	public void test() { System.out.println("test()"); }
	public void test(int i, int s) { System.out.println("test("+i+","+s+")"); }
	
	public static void main(String...args) {
		Model.launch(new MultiargsMessage());
	}
}
