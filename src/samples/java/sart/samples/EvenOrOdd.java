package sart.samples;

import java.util.stream.LongStream;

import sart.actor.BasicActor;
import sart.actor.Model;

public class EvenOrOdd extends BasicActor {

	@Override
	public void define() {
		behavior("IDLE").
			Case("even|odd", "isEven", long.class);
		
		behavior("IDLE0").
			Case("even", "isEven", long.class).
			Case("odd", long.class);
		
		become("IDLE");//same IDLE0
		LongStream.range(0, 256).forEach(this::send);
	}
	
	public void even(long v) {
		System.out.println(v + " is even");
	}
	
	public void odd(long v) {
		System.out.println(v + " is odd");
	}
	
	public boolean isEven(long v) {
		return v % 2 == 0;
	}
	
	public static void main(String...args) {
		Model.launch(new EvenOrOdd());
	}
}
