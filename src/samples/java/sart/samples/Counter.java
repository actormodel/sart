package sart.samples;

import sart.actor.BasicActor;
import sart.actor.Model;

import static sart.actor.BehaviorBuilder.*;

public class Counter extends BasicActor {
	private static final String MTick = "tick", MPrint = "print";
	
	private int count = 0;
	
	@Override
	public void define() {
		behavior("IDLE").
			Case("tick", tsig(String.class), vsig(MTick)).
			Case("print", tsig(String.class), vsig(MPrint));
		become("IDLE");
		send(MTick);
		send(M(MPrint), 1000);
	}
	
	public void tick() {
		count++;
		send(MTick);
	}
	
	public void print() {
		System.out.println(count + 1);// + 1 include MPrint message
		count = 0;
		send(M(MPrint), 1000);
	}
	
	public static void main(String...args) {
		Model.launch(new Counter());
	}
}
